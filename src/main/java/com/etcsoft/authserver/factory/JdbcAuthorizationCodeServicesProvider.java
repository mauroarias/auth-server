package com.etcsoft.authserver.factory;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;

import javax.sql.DataSource;

import static com.etcsoft.authserver.factory.HikariDataSourcesProvider.USER_DATASOURCE;

@Configuration
public class JdbcAuthorizationCodeServicesProvider {

    private final DataSource dataSource;

    @Autowired
    JdbcAuthorizationCodeServicesProvider(@Qualifier(USER_DATASOURCE) final HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    protected AuthorizationCodeServices authorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(dataSource);
    }
}
