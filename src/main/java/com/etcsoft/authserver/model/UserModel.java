package com.etcsoft.authserver.model;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Value
@Builder
public class UserModel {
    private String username;
    private String password;
    @Singular
    private List<GrantedAuthority> authorities;
}
