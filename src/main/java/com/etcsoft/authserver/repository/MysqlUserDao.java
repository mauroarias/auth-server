package com.etcsoft.authserver.repository;

import com.etcsoft.authserver.model.UserModel;
import com.etcsoft.authserver.model.exception.DatabaseException;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

import static com.etcsoft.authserver.factory.HikariDataSourcesProvider.USER_DATASOURCE;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Repository
public class MysqlUserDao implements UserDao {
    private final static Logger logger = Logger.getLogger(MysqlUserDao.class);

    private final static String FIND_USER_QUERY = "SELECT * FROM users WHERE username=?";

    private final HikariDataSource dataSource;

    @Autowired
    MysqlUserDao(@Qualifier(USER_DATASOURCE) HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    @SneakyThrows
    public Optional<UserModel> getUser(String username) {

        try (Connection connection = dataSource.getConnection()) {
            try {
                logger.debug(format("Performing a select username: %s", username));

                PreparedStatement selectStatement = connection.prepareStatement(FIND_USER_QUERY);
                selectStatement.setString(1, username);
                final ResultSet resultSet = selectStatement.executeQuery();
                if (resultSet.next() && resultSet.getBoolean("enable")) {
                    return of(UserModel
                            .builder()
                            .username(username)
                            .authority(new SimpleGrantedAuthority("ROLE_SYSTEMADMIN"))
                            .password(resultSet.getString("password"))
                            .build());
                }
                return empty();

            } catch (Exception ex) {
                throw new DatabaseException("Error selecting table", ex);
            }
        }
    }
}