package com.etcsoft.authserver.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class SecurityJwtConfig {
    private final String signedKey;
    private final boolean signed;

    public SecurityJwtConfig(final @Value("${security.jwt.signing-key}") String signedKey,
                             final @Value("${security.jwt.signed}") boolean signed) {
        this.signedKey = signedKey;
        this.signed = signed;
    }
}
