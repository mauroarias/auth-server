package com.etcsoft.authserver.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class SecurityMysqlUserEndPoint {
    private final String user;
    private final String passwd;
    private final String host;
    private final String databaseName;
    private final int port;

    public SecurityMysqlUserEndPoint(final @Value("${security.mysql.user.username}") String mysqlUser,
                                     final @Value("${security.mysql.user.password}") String mysqlPasswd,
                                     final @Value("${security.mysql.user.host}") String mysqlHost,
                                     final @Value("${security.mysql.user.database}") String databaseName,
                                     final @Value("${security.mysql.user.port}") Integer mysqlPort) {
        this.host = mysqlHost;
        this.passwd = mysqlPasswd;
        this.port = mysqlPort == null ? 3306 : mysqlPort;
        this.user = mysqlUser;
        this.databaseName = databaseName;
    }
}
