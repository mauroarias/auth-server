package com.etcsoft.authserver.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;
import java.util.Arrays;

import static com.etcsoft.authserver.factory.HikariDataSourcesProvider.OAUTH2_DATASOURCE;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfigAdapter extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authenticationManager;
    private final TokenStore tokenStore;
    private final JwtAccessTokenConverter jwtTokenConverter;
    private final DataSource oauth2DataSource;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    AuthorizationServerConfigAdapter(final TokenStore tokenStore,
                                     final JwtAccessTokenConverter jwtTokenConverter,
                                     final AuthenticationManager authenticationManager,
                                     final PasswordEncoder passwordEncoder,
                                     @Qualifier(OAUTH2_DATASOURCE) final HikariDataSource oauth2DataSource) {
        this.tokenStore = tokenStore;
        this.jwtTokenConverter = jwtTokenConverter;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.oauth2DataSource = oauth2DataSource;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        enhancerChain.setTokenEnhancers(Arrays.asList(jwtTokenConverter));

        endpoints
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore)
                .accessTokenConverter(jwtTokenConverter)
                .tokenEnhancer(enhancerChain);
    }

//    @Override
//    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
//        security
//                .tokenKeyAccess("permitAll()")
//                .checkTokenAccess("isAuthenticated()")
//                .allowFormAuthenticationForClients();
//    }
//
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        clients.jdbc(oauth2DataSource).passwordEncoder(passwordEncoder);
    }

}