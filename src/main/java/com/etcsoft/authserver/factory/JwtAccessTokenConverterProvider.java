package com.etcsoft.authserver.factory;

import com.etcsoft.authserver.config.SecurityJwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
public class JwtAccessTokenConverterProvider {

    private final SecurityJwtConfig signatureKeyConfig;

    @Autowired
    JwtAccessTokenConverterProvider(final SecurityJwtConfig signatureKeyConfig) {
        this.signatureKeyConfig = signatureKeyConfig;
    }

    @Bean
    public JwtAccessTokenConverter tokenEnhancer() {
        final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        if(signatureKeyConfig.isSigned()) {
            converter.setSigningKey(signatureKeyConfig.getSignedKey());
        }
        return converter;
    }
}
