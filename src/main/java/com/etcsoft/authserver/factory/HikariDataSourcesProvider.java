package com.etcsoft.authserver.factory;

import com.etcsoft.authserver.config.SecurityMysqlOauth2EndPoint;
import com.etcsoft.authserver.config.SecurityMysqlUserEndPoint;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class HikariDataSourcesProvider {
    public final static String USER_DATASOURCE = "userDatasource";
    public final static String OAUTH2_DATASOURCE = "oauth2Datasource";

    private final SecurityMysqlOauth2EndPoint oauth2Config;
    private final SecurityMysqlUserEndPoint userConfig;

    @Autowired
    HikariDataSourcesProvider(final SecurityMysqlOauth2EndPoint oauth2Config,
                              final SecurityMysqlUserEndPoint userConfig) {
        this.oauth2Config = oauth2Config;
        this.userConfig = userConfig;
    }

    @Bean(name = USER_DATASOURCE, destroyMethod = "close")
    public HikariDataSource createUserDatasource() {
        return createDatasource(userConfig.getUser(),
                                userConfig.getPasswd(),
                                userConfig.getHost(),
                                userConfig.getDatabaseName(),
                                userConfig.getPort());
    }

    @Bean(name = OAUTH2_DATASOURCE, destroyMethod = "close")
    @Primary
    public HikariDataSource createOauth2Datasource() {
        return createDatasource(oauth2Config.getUser(),
                                oauth2Config.getPasswd(),
                                oauth2Config.getHost(),
                                oauth2Config.getDatabaseName(),
                                oauth2Config.getPort());
    }

    private HikariDataSource createDatasource(final String user,
                                              final String passwd,
                                              final String host,
                                              final String database,
                                              final Integer port) {
        final HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl(String.format("jdbc:mysql://%s:%d/%s", host, port, database));
        dataSource.setUsername(user);
        dataSource.setPassword(passwd);
        dataSource.setMaximumPoolSize(5);
        dataSource.setPoolName("springHikariCP");
        dataSource.setAutoCommit(false);
        return dataSource;
    }
}
