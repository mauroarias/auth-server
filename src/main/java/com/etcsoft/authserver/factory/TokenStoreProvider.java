package com.etcsoft.authserver.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class TokenStoreProvider {

    private final JwtAccessTokenConverter tokenConverter;

    @Autowired
    TokenStoreProvider(final JwtAccessTokenConverter tokenConverter) {
        this.tokenConverter = tokenConverter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(tokenConverter);
    }
}
