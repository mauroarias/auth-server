package com.etcsoft.authserver.repository;

import com.etcsoft.authserver.model.UserModel;

import java.util.Optional;

public interface UserDao {

    Optional<UserModel> getUser(String username);
}
