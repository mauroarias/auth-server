package com.etcsoft.authserver.service;

import com.etcsoft.authserver.model.CustomUser;
import com.etcsoft.authserver.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserDao userDAO;

    @Autowired
    CustomUserDetailsService(final UserDao userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        return userDAO
                .getUser(username)
                .map(CustomUser::new)
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));
    }

}